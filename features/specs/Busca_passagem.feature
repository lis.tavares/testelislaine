#language: pt
Funcionalidade: Busca de Passagem Áerea
- Como cliente
- Eu desejo buscar voos
- Assim que selecionar minhas opções de Origem, Destino e Datas.

Esquema do Cenario: Efetuar busca de passagem área

    Dado que eu esteja na pagina da latam
    Quando no menu de pesquisa informo a <origem>, <destino>, <data_ida>, <data_volta> e <qtd_passageiro>
    E clico no botão "Procurar"
    Então é apresentado o resultado da pesquisa

Massas:
    | origem      | destino          | data_ida     | data_volta   |
    | "São Paulo" | "Rio de Janeiro" | "Qua 01 set" | "Qua 11 set" |
    