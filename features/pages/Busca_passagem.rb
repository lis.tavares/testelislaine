class Busca_passagem < SitePrism::Page
    set_url '/br/pt'
    element :inputInputOrigin, 'input[id="lblInputOrigin"]'
    element :lblInputDestination, 'input[id="lblInputDestination"]'
    element :btnItemAutoComplete_0, 'button[id="btnItemAutoComplete_0"]'
    element :btnContinueCTA, 'button[id="btnContinueCalendarCTA"]'
    element :InputFalseStyled, 'div[id="InputFalseStyled"]'
    element :btnAddPassengerCTA, 'div[id="btnAddPassengerCTA"]'
    element :btnaddAdult, 'button[id="addAdult"]'
      
    
    element :btnsendCallback, 'button[id="sendCallback"]'
    element :btnSearchCTA, 'div[id="btnSearchCTA""]'
    element :spanTxtIda, 'span[class="sc-cFlXAS dTkFnd"]'
    element :spanTxtVolta, 'span[class="sc-cFlXAS dTkFnd"]'
    element :btnCookiespolitics, 'button[id="cookies-politics-button"]'
    

    def click_button_login
        btnLogin.click
    end

    def preenche_campos(origem, destino, data_ida, data_volta, qtd_passageiro)
         inputInputOrigin.set origem
         lblInputDestination.set destino
         btnItemAutoComplete_0.set data_ida
         InputFalseStyled.set data_volta
         btnAddPassengerCTA.set qtd_passageiro
        
    end

    
    def button_click(bt_continuar)
        puts bt_continuar
        btnContinueCTA.click
    end

    def button_click(add_passageiro) /duvida/
        puts add_pasdageiro
        btnAddPassengerCTA.click
    end

    def button_click(add_adulto)
        puts add_passageiro
        btnaddAdult.click
    end

    def button_click(send_callback)
        puts send_callback
        btnsendCallback.click
    end

    def button_click(bt_procurar)
        puts bt_procurar
        btnSearchCTA.click
    end

    def confirma_busca(txt_ida, txt_volta)
        txtExpected = "GRU", "SDU"   
        if txtUserName.text.eql?(txtExpected) != true
            raise "Esperado: #{txtExpected}, mas retornou #{txtUserName.text}." 
        end
    end

   
   def button_click(bt_cookies)
    puts bt_cookies
    btnCookiespolitics.click
   end  
    
   
end